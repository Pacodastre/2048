# 2048

This repository is a way for the contributors to write different implementations of the 2048 game.
This repository currently hosts one version of the 2048 game clone written in JavaScript using the React framework.
See the README file in the `2048-react` for more information.

