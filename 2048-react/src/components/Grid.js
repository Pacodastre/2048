import { Table, Button } from "semantic-ui-react";
import { useState, useEffect } from "react";
import {
  placeRandomNumber,
  moveUp,
  moveDown,
  moveLeft,
  checkGridsAreEqual,
  moveRight,
  checkGameOver,
} from "lib/grid";
import GameOverPopup from "./GameOverPopup";

function Grid() {
  const [grid, setGrid] = useState([
    [null, null, null, null],
    [null, null, null, null],
    [null, null, null, null],
    [null, null, null, null],
  ]);
  const [showGameOver, setShowGameOver] = useState(false);

  useEffect(() => {
    setGrid(placeRandomNumber(grid));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  function handleClickUp() {
    const movedGrid = moveUp(grid);

    if (!checkGridsAreEqual(grid, movedGrid)) {
      const newGrid = placeRandomNumber(movedGrid);
      setGrid(newGrid);
      if (checkGameOver(newGrid)) {
        setShowGameOver(true);
      }
    }
  }

  function handleClickDown() {
    const movedGrid = moveDown(grid);

    if (!checkGridsAreEqual(grid, movedGrid)) {
      const newGrid = placeRandomNumber(movedGrid);
      setGrid(newGrid);
      if (checkGameOver(newGrid)) {
        setShowGameOver(true);
      }
    }
  }

  function handleClickLeft() {
    const movedGrid = moveLeft(grid);

    if (!checkGridsAreEqual(grid, movedGrid)) {
      const newGrid = placeRandomNumber(movedGrid);
      setGrid(newGrid);
      if (checkGameOver(newGrid)) {
        setShowGameOver(true);
      }
    }
  }

  function handleClickRight() {
    const movedGrid = moveRight(grid);

    if (!checkGridsAreEqual(grid, movedGrid)) {
      const newGrid = placeRandomNumber(movedGrid);
      setGrid(newGrid);
      if (checkGameOver(newGrid)) {
        setShowGameOver(true);
      }
    }
  }

  return (
    <>
      <div className="table-wrapper">
        <Table celled>
          <Table.Body>
            {grid.map((row, rowIndex) => (
              <Table.Row key={rowIndex}>
                {row.map((cell, cellIndex) => (
                  <Table.Cell key={cellIndex} className="table-cell">
                    {cell}
                  </Table.Cell>
                ))}
              </Table.Row>
            ))}
          </Table.Body>
        </Table>
        {showGameOver && (
          <GameOverPopup setGrid={setGrid} setShowGameOver={setShowGameOver} />
        )}
        <div className="buttons">
          <Button onClick={handleClickUp}>Up</Button>
          <Button onClick={handleClickDown}>Down</Button>
          <Button onClick={handleClickLeft}>Left</Button>
          <Button onClick={handleClickRight}>Right</Button>
        </div>
      </div>
    </>
  );
}
export default Grid;
