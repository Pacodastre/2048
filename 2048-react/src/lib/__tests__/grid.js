import {
  copyMultidimensionalArray,
  placeRandomNumber,
  getFreeCells,
  isCellFree,
  getRandomFreeCell,
  getHighestFreeCellInColumn,
  getLowestFreeCellInColumn,
  getLeftmostFreeCellInRow,
  getRightmostFreeCellInRow,
  moveUp,
  moveCellUp,
  moveCellDown,
  moveCellRight,
  mergeCellsUp,
  mergeCellsLeft,
  mergeCellsRight,
} from "lib/grid";

it("makes a deep copy of an empty 2 dimensional array", () => {
  const array = [[], [], [], []];
  const arrayCopy = copyMultidimensionalArray(array);
  expect(array).toEqual(arrayCopy);
  expect(array).not.toBe(arrayCopy);
});

it("makes a deep copy of a 2 dimensional array with data", () => {
  const array = [
    [1, 2, 3, 4],
    [5, 6, 7, 8],
    [9, 10, 11, 12],
    [13, 14, 15, 16],
  ];
  const arrayCopy = copyMultidimensionalArray(array);
  expect(array).toEqual(arrayCopy);
  expect(array).not.toBe(arrayCopy);
});

it("checks cell is empty on an empty grid", () => {
  const grid = [
    [null, null, null, null],
    [null, null, null, null],
    [null, null, null, null],
    [null, null, null, null],
  ];
  expect(isCellFree(grid, [0, 1])).toEqual(true);
});
it("checks cell is full on an full grid", () => {
  const grid = [
    [2, 2, 2, 2],
    [2, 2, 2, 2],
    [2, 2, 2, 2],
    [2, 2, 2, 2],
  ];
  expect(isCellFree(grid, [0, 1])).toEqual(false);
});
it("checks cell is empty on an partially filled grid", () => {
  const grid = [
    [2, 2, null, 2],
    [null, null, 2, 2],
    [2, null, 2, 2],
    [2, 2, 2, null],
  ];
  expect(isCellFree(grid, [2, 1])).toEqual(true);
});
it("checks cell is empty on an grid with one empty cell", () => {
  const grid = [
    [2, 2, 2, 2],
    [2, 2, 2, 2],
    [2, 2, 2, 2],
    [2, 2, 2, null],
  ];
  expect(isCellFree(grid, [3, 3])).toEqual(true);
});

it("places a random number on an empty grid", () => {
  const grid = [
    [null, null, null, null],
    [null, null, null, null],
    [null, null, null, null],
    [null, null, null, null],
  ];
  const updatedGrid = placeRandomNumber(grid);
  const freeCells = getFreeCells(updatedGrid);
  // 15 because 16 - the new tile
  expect(freeCells.length).toEqual(15);
});

it("places a random number on grid containing 2 tiles", () => {
  const grid = [
    [2, null, null, null],
    [null, null, null, null],
    [null, null, null, null],
    [null, 2, null, null],
  ];
  const updatedGrid = placeRandomNumber(grid);
  const freeCells = getFreeCells(updatedGrid);
  // 13 because there are 2 tiles already, plus the new one
  expect(freeCells.length).toEqual(13);
});

it("gets the list of free cells in an empty grid", () => {
  const grid = [
    [null, null, null, null],
    [null, null, null, null],
    [null, null, null, null],
    [null, null, null, null],
  ];
  const expectedEmptyCells = [
    [0, 0],
    [0, 1],
    [0, 2],
    [0, 3],
    [1, 0],
    [1, 1],
    [1, 2],
    [1, 3],
    [2, 0],
    [2, 1],
    [2, 2],
    [2, 3],
    [3, 0],
    [3, 1],
    [3, 2],
    [3, 3],
  ];
  expect(getFreeCells(grid)).toEqual(expectedEmptyCells);
});

it("gets the list of free cells in a half full grid", () => {
  const grid = [
    [null, null, null, null],
    [4, null, null, null],
    [4, 8, null, null],
    [8, 16, 32, 64],
  ];
  const expectedEmptyCells = [
    [0, 0],
    [0, 1],
    [0, 2],
    [0, 3],
    [1, 1],
    [1, 2],
    [1, 3],
    [2, 2],
    [2, 3],
  ];
  expect(getFreeCells(grid)).toEqual(expectedEmptyCells);
});

it("gets the list of free cells in a full grid", () => {
  const grid = [
    [2, 2, 2, 2],
    [4, 4, 4, 4],
    [4, 8, 8, 8],
    [8, 16, 32, 64],
  ];
  const expectedEmptyCells = [];
  expect(getFreeCells(grid)).toEqual(expectedEmptyCells);
});

it("Gets an empty cell from a grid with one empty cell", () => {
  const grid = [
    [2, 2, 2, null],
    [2, 2, 2, 2],
    [2, 2, 2, 2],
    [2, 2, 2, 2],
  ];
  expect(getRandomFreeCell(grid)).toEqual([0, 3]);
});

it("getRandomFreeCell returns an empty array when grid is full", () => {
  const grid = [
    [2, 2, 2, 2],
    [2, 2, 2, 2],
    [2, 2, 2, 2],
    [2, 2, 2, 2],
  ];
  expect(getRandomFreeCell(grid)).toEqual(null);
});
// moveUp
it("moveUp moves up any cells which have space above", () => {
  const grid = [
    [2, null, null, null],
    [null, 2, null, null],
    [null, null, 2, null],
    [null, null, null, 2],
  ];
  expect(moveUp(grid)).toEqual([
    [2, 2, 2, 2],
    [null, null, null, null],
    [null, null, null, null],
    [null, null, null, null],
  ]);
});
it("moveUp moves up any cells which have space above, with multiple values per column", () => {
  const grid = [
    [2, null, null, null],
    [null, 2, null, 2],
    [null, 2, 2, null],
    [2, null, null, 2],
  ];
  expect(moveUp(grid)).toEqual([
    [4, 4, 2, 4],
    [null, null, null, null],
    [null, null, null, null],
    [null, null, null, null],
  ]);
});
it("moveUp doesnt change column if full", () => {
  const grid = [
    [2, null, null, null],
    [4, 2, null, 2],
    [2, 2, 2, null],
    [4, null, null, 2],
  ];
  expect(moveUp(grid)).toEqual([
    [2, 4, 2, 4],
    [4, null, null, null],
    [2, null, null, null],
    [4, null, null, null],
  ]);
});
it("moveUp well merge same cells even if not adjacent", () => {
  const grid = [
    [null, 2, null, null],
    [2, null, null, null],
    [null, 2, null, null],
    [2, null, null, 2],
  ];
  expect(moveUp(grid)).toEqual([
    [4, 4, null, 2],
    [null, null, null, null],
    [null, null, null, null],
    [null, null, null, null],
  ]);
});

// moveCellUp
it("moveUp moves up one cell", () => {
  const grid = [
    [null, null, null, null],
    [null, 2, null, null],
    [null, null, null, null],
    [null, null, null, null],
  ];
  expect(moveCellUp(grid, [1, 1])).toEqual([
    [null, 2, null, null],
    [null, null, null, null],
    [null, null, null, null],
    [null, null, null, null],
  ]);
});
it("moveUp doesnt affect row 1", () => {
  const grid = [
    [null, 2, null, null],
    [null, null, null, null],
    [null, null, null, null],
    [null, null, null, null],
  ];
  expect(moveCellUp(grid, [1, 1])).toEqual([
    [null, 2, null, null],
    [null, null, null, null],
    [null, null, null, null],
    [null, null, null, null],
  ]);
});
it("moveUp moves cell to highest available empty cell", () => {
  const grid = [
    [null, 2, null, null],
    [null, null, null, null],
    [null, null, null, null],
    [null, 2, null, null],
  ];
  expect(moveCellUp(grid, [3, 1])).toEqual([
    [null, 2, null, null],
    [null, 2, null, null],
    [null, null, null, null],
    [null, null, null, null],
  ]);
});

// getHighestFreeCellInColumn
it("getHighestFreeCellInColumn returns top cell index, if one cell above is empty", () => {
  const grid = [
    [null, null, null, null],
    [null, 2, null, null],
    [null, null, null, null],
    [null, null, null, null],
  ];
  expect(getHighestFreeCellInColumn(grid, [1, 1])).toEqual([0, 1]);
});
it("getHighestFreeCellInColumn returns top cell index, if two cells above are empty", () => {
  const grid = [
    [null, null, null, null],
    [null, null, null, null],
    [null, 2, null, null],
    [null, null, null, null],
  ];
  expect(getHighestFreeCellInColumn(grid, [2, 1])).toEqual([0, 1]);
});
it("getHighestFreeCellInColumn returns top cell index, if three cells above are empty", () => {
  const grid = [
    [null, null, null, null],
    [null, null, null, null],
    [null, null, null, null],
    [null, 2, null, null],
  ];
  expect(getHighestFreeCellInColumn(grid, [3, 1])).toEqual([0, 1]);
});
it("returns highest empty cell in a column", () => {
  const grid = [
    [null, 4, null, null],
    [null, null, null, null],
    [null, 2, null, null],
    [null, null, null, null],
  ];
  expect(getHighestFreeCellInColumn(grid, [2, 1])).toEqual([1, 1]);
});
it("getFreeCellAbovest returns the only empty cell index in a row with one empty cell", () => {
  const grid = [
    [null, 2, null, null],
    [null, 2, null, null],
    [null, null, null, null],
    [null, 2, null, null],
  ];
  expect(getHighestFreeCellInColumn(grid, [3, 1])).toEqual([2, 1]);
});
it("getHighestFreeCellInColumn returns current cell index if no empty cells exist above", () => {
  const grid = [
    [null, 2, null, null],
    [null, 2, null, null],
    [null, null, null, null],
    [null, null, null, null],
  ];
  expect(getHighestFreeCellInColumn(grid, [1, 1])).toEqual([1, 1]);
});

// mergeCellsMovingUp
it("Checks if cell below is equal to current cell being checks, and if equal, merges them together", () => {
  const grid = [
    [null, 2, null, null],
    [null, 2, null, null],
    [null, null, null, null],
    [null, null, null, null],
  ];
  expect(mergeCellsUp(grid, [0, 1])).toEqual([
    [null, 4, null, null],
    [null, null, null, null],
    [null, null, null, null],
    [null, null, null, null],
  ]);
});

// **********************************
//         Move Down Stuff
// **********************************

// getLowestFreeCellInColumn
it("getLowestFreeCellInColumn returns bottom cell index, if one cell below is empty", () => {
  const grid = [
    [null, null, null, null],
    [null, null, null, null],
    [null, 2, null, null],
    [null, null, null, null],
  ];
  expect(getLowestFreeCellInColumn(grid, [2, 1])).toEqual([3, 1]);
});
it("getLowestFreeCellInColumn returns bottom cell index, if two cells below are empty", () => {
  const grid = [
    [null, null, null, null],
    [null, 2, null, null],
    [null, null, null, null],
    [null, null, null, null],
  ];
  expect(getLowestFreeCellInColumn(grid, [1, 1])).toEqual([3, 1]);
});
it("getLowestFreeCellInColumn returns bottom cell index, if three cells below are empty", () => {
  const grid = [
    [null, 2, null, null],
    [null, null, null, null],
    [null, null, null, null],
    [null, null, null, null],
  ];
  expect(getLowestFreeCellInColumn(grid, [0, 1])).toEqual([3, 1]);
});
it("returns lowest empty cell in a column", () => {
  const grid = [
    [null, null, null, null],
    [null, 2, null, null],
    [null, null, null, null],
    [null, 4, null, null],
  ];
  expect(getLowestFreeCellInColumn(grid, [1, 1])).toEqual([2, 1]);
});
it("getLowestFreeCellInColumn returns the only empty cell index in a row with one empty cell", () => {
  const grid = [
    [null, 2, null, null],
    [null, 2, null, null],
    [null, null, null, null],
    [null, 2, null, null],
  ];
  expect(getLowestFreeCellInColumn(grid, [1, 1])).toEqual([2, 1]);
});
it("getLowestFreeCellInColumn returns current cell index if no empty cells exist below", () => {
  const grid = [
    [null, null, null, null],
    [null, null, null, null],
    [null, 2, null, null],
    [null, 2, null, null],
  ];
  expect(getLowestFreeCellInColumn(grid, [2, 1])).toEqual([2, 1]);
});
// moveCellDown
it("cell moves to bottom row, if corresponding bottom row cell is empty", () => {
  const grid = [
    [null, null, null, null],
    [null, null, null, null],
    [null, 2, null, null],
    [null, null, null, null],
  ];
  expect(moveCellDown(grid, [2, 1])).toEqual([
    [null, null, null, null],
    [null, null, null, null],
    [null, null, null, null],
    [null, 2, null, null],
  ]);
});
it("moveDown doesn't affect the bottom row.", () => {
  const grid = [
    [null, null, null, null],
    [null, null, null, null],
    [null, null, null, null],
    [null, 2, null, null],
  ];
  expect(moveCellDown(grid, [3, 1])).toEqual([
    [null, null, null, null],
    [null, null, null, null],
    [null, null, null, null],
    [null, 2, null, null],
  ]);
});
it("moveDown moves cell to lowest available empty cell", () => {
  const grid = [
    [null, 2, null, null],
    [null, null, null, null],
    [null, null, null, null],
    [null, 2, null, null],
  ];
  expect(moveCellDown(grid, [0, 1])).toEqual([
    [null, null, null, null],
    [null, null, null, null],
    [null, 2, null, null],
    [null, 2, null, null],
  ]);
});

// *******************************
//    Left Functionality Tests
// *******************************

// getLeftmostFreeCellInRow
it("getLeftmostFreeCellInRow returns leftmost cell index, if checking the second index in a row", () => {
  const grid = [
    [null, null, null, null],
    [null, null, null, null],
    [null, 2, null, null],
    [null, null, null, null],
  ];
  expect(getLeftmostFreeCellInRow(grid, [2, 1])).toEqual([2, 0]);
});
it("getLeftmostFreeCellInRow returns leftmost cell index, if checking the third index in a row", () => {
  const grid = [
    [null, null, null, null],
    [null, null, 2, null],
    [null, null, null, null],
    [null, null, null, null],
  ];
  expect(getLeftmostFreeCellInRow(grid, [1, 2])).toEqual([1, 0]);
});
it("getLeftmostFreeCellInRow returns leftmost cell index, if checking the fourth index in a row", () => {
  const grid = [
    [null, null, null, 2],
    [null, null, null, null],
    [null, null, null, null],
    [null, null, null, null],
  ];
  expect(getLeftmostFreeCellInRow(grid, [0, 3])).toEqual([0, 0]);
});
it("getLeftmostFreeCellInRow returns leftmost empty cell in a column with multiple numbers", () => {
  const grid = [
    [null, null, null, null],
    [null, 2, null, 4],
    [null, null, null, null],
    [null, null, null, null],
  ];
  expect(getLeftmostFreeCellInRow(grid, [1, 1])).toEqual([1, 0]);
});
it("getLeftmostFreeCellInRow returns the only empty cell index in a row with one empty cell", () => {
  const grid = [
    [null, null, null, null],
    [2, null, 2, 2],
    [null, null, null, null],
    [null, null, null, null],
  ];
  expect(getLeftmostFreeCellInRow(grid, [1, 2])).toEqual([1, 1]);
});
it("getLeftmostFreeCellInRow returns current cell index if no empty cells exist to the left", () => {
  const grid = [
    [null, null, null, null],
    [null, null, null, null],
    [2, 2, null, null],
    [null, null, null, null],
  ];
  expect(getLeftmostFreeCellInRow(grid, [2, 1])).toEqual([2, 1]);
});

// MergeCellsLeft
// mergeCellsMovingUp
it("Checks if cell to right is equal to current cell being checks, and if equal, merges them together", () => {
  const grid = [
    [null, null, null, null],
    [2, 2, null, null],
    [null, null, null, null],
    [null, null, null, null],
  ];
  expect(mergeCellsLeft(grid, [1, 0])).toEqual([
    [null, null, null, null],
    [4, null, null, null],
    [null, null, null, null],
    [null, null, null, null],
  ]);
});

it("merges two equal cells in the same row", () => {
  const grid = [
    [null, null, null, null],
    [null, 2, null, 2],
    [null, null, null, null],
    [null, null, null, null],
  ];
  expect(mergeCellsLeft(grid, [1, 1])).toEqual([
    [null, null, null, null],
    [null, 4, null, null],
    [null, null, null, null],
    [null, null, null, null],
  ]);
});
it("merges first two equal cells in the same row", () => {
  const grid = [
    [null, null, null, null],
    [2, 2, null, 2],
    [null, null, null, null],
    [null, null, null, null],
  ];
  expect(mergeCellsLeft(grid, [1, 0])).toEqual([
    [null, null, null, null],
    [4, null, null, 2],
    [null, null, null, null],
    [null, null, null, null],
  ]);
});

it("merges two equal cells in the same row containing other numbers", () => {
  const grid = [
    [null, null, null, null],
    [4, 8, 4, 4],
    [null, null, null, null],
    [null, null, null, null],
  ];
  expect(mergeCellsLeft(grid, [1, 2])).toEqual([
    [null, null, null, null],
    [4, 8, 8, null],
    [null, null, null, null],
    [null, null, null, null],
  ]);
});

// *******************************
//    Right Functionality Tests
// *******************************

// getRightmostFreeCellInRow
it("getRightmostFreeCellInRow returns rightmost cell index, if checking the second index from the right", () => {
  const grid = [
    [null, null, null, null],
    [null, null, null, null],
    [null, null, 2, null],
    [null, null, null, null],
  ];
  expect(getRightmostFreeCellInRow(grid, [2, 2])).toEqual([2, 3]);
});
it("getRightmostFreeCellInRow returns rightmost cell index, if checking the second cell from the right", () => {
  const grid = [
    [null, null, null, null],
    [null, 2, null, null],
    [null, null, null, null],
    [null, null, null, null],
  ];
  expect(getRightmostFreeCellInRow(grid, [1, 1])).toEqual([1, 3]);
});
it("getRightmostFreeCellInRow returns rightmost cell index, if checking the first cell in row", () => {
  const grid = [
    [2, null, null, null],
    [null, null, null, null],
    [null, null, null, null],
    [null, null, null, null],
  ];
  expect(getRightmostFreeCellInRow(grid, [0, 0])).toEqual([0, 3]);
});
it("getRightmostFreeCellInRow returns rightmost empty cell in a column with multiple numbers", () => {
  const grid = [
    [null, null, null, null],
    [null, 2, null, 4],
    [null, null, null, null],
    [null, null, null, null],
  ];
  expect(getRightmostFreeCellInRow(grid, [1, 1])).toEqual([1, 2]);
});
it("getRightmostFreeCellInRow returns the only empty cell index in a row with one empty cell", () => {
  const grid = [
    [null, null, null, null],
    [2, null, 2, 2],
    [null, null, null, null],
    [null, null, null, null],
  ];
  expect(getRightmostFreeCellInRow(grid, [1, 0])).toEqual([1, 1]);
});
it("getRightmostFreeCellInRow returns current cell index if no empty cells exist to the right", () => {
  const grid = [
    [null, null, null, null],
    [null, null, null, null],
    [null, null, 2, 2],
    [null, null, null, null],
  ];
  expect(getRightmostFreeCellInRow(grid, [2, 2])).toEqual([2, 2]);
});

// moveCellRight
it(" moves right one cell", () => {
  const grid = [
    [null, null, null, null],
    [null, null, 2, null],
    [null, null, null, null],
    [null, null, null, null],
  ];
  expect(moveCellRight(grid, [1, 2])).toEqual([
    [null, null, null, null],
    [null, null, null, 2],
    [null, null, null, null],
    [null, null, null, null],
  ]);
});
it("doesnt affect last cell in row", () => {
  const grid = [
    [null, 2, null, null],
    [null, null, null, 2],
    [null, null, null, null],
    [null, null, null, null],
  ];
  expect(moveCellRight(grid, [1, 3])).toEqual([
    [null, 2, null, null],
    [null, null, null, 2],
    [null, null, null, null],
    [null, null, null, null],
  ]);
});
it("moves cell to rightmost available empty cell", () => {
  const grid = [
    [null, 2, null, null],
    [null, null, null, null],
    [null, null, null, null],
    [2, null, null, 2],
  ];
  expect(moveCellRight(grid, [3, 0])).toEqual([
    [null, 2, null, null],
    [null, null, null, null],
    [null, null, null, null],
    [null, null, 2, 2],
  ]);
});

// mergeCellsRight
it("Checks if cell to left is equal to current cell being checked, and if equal, merges them together", () => {
  const grid = [
    [null, null, null, null],
    [null, null, 2, 2],
    [null, null, null, null],
    [null, null, null, null],
  ];
  expect(mergeCellsRight(grid, [1, 3])).toEqual([
    [null, null, null, null],
    [null, null, null, 4],
    [null, null, null, null],
    [null, null, null, null],
  ]);
});

it("merges two cells in the same row", () => {
  const grid = [
    [null, null, null, null],
    [2, null, 2, null],
    [null, null, null, null],
    [null, null, null, null],
  ];
  expect(mergeCellsRight(grid, [1, 2])).toEqual([
    [null, null, null, null],
    [null, null, 4, null],
    [null, null, null, null],
    [null, null, null, null],
  ]);
});
it("Merges first two equal cells in the same row", () => {
  const grid = [
    [null, null, null, null],
    [2, null, 2, 2],
    [null, null, null, null],
    [null, null, null, null],
  ];
  expect(mergeCellsRight(grid, [1, 3])).toEqual([
    [null, null, null, null],
    [2, null, null, 4],
    [null, null, null, null],
    [null, null, null, null],
  ]);
});

it("merges 2 equal cells in the same row containing other numbers", () => {
  const grid = [
    [null, null, null, null],
    [4, 4, 2, 8],
    [null, null, null, null],
    [null, null, null, null],
  ];
  expect(mergeCellsRight(grid, [1, 1])).toEqual([
    [null, null, null, null],
    [null, 8, 2, 8],
    [null, null, null, null],
    [null, null, null, null],
  ]);
});
