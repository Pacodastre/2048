export function copyMultidimensionalArray(grid) {
  let grid2 = [];
  for (let i = 0; i < grid.length; i++) grid2[i] = grid[i].slice();
  return grid2;
}

export function isCellFree(currentGrid, [row, cell]) {
  return currentGrid[row][cell] === null;
}

export function getFreeCells(grid) {
  let freeCells = [];
  grid.forEach((row, rowIndex) => {
    row.forEach((cell, cellIndex) => {
      if (isCellFree(grid, [rowIndex, cellIndex])) {
        freeCells.push([rowIndex, cellIndex]);
      }
    });
  });
  return freeCells;
}

export function getRandomFreeCell(currentGrid) {
  const freeCells = getFreeCells(currentGrid);
  const randomCell = freeCells[Math.floor(Math.random() * freeCells.length)];

  return !freeCells.length ? null : randomCell;
}

export function placeRandomNumber(currentGrid) {
  const randomCell = getRandomFreeCell(currentGrid);
  let newGrid = copyMultidimensionalArray(currentGrid);

  newGrid[randomCell[0]][randomCell[1]] = 2;

  return newGrid;
}

export function mergeCellsUp(grid, [rowIndex, cellIndex]) {
  const cellValue = grid[rowIndex][cellIndex];

  for (let n = rowIndex + 1; n < grid.length; n++) {
    if (cellValue && cellValue !== grid[n][cellIndex] && grid[n][cellIndex]) {
      break;
    } else if (cellValue === grid[n][cellIndex]) {
      grid[rowIndex][cellIndex] = cellValue + grid[n][cellIndex];
      grid[n][cellIndex] = null;
    }
  }
  return grid;
}

export function moveUp(grid) {
  let movedArray = copyMultidimensionalArray(grid);

  const start = 0;
  const end = movedArray.length;

  for (let i = start; i < end; i++) {
    const row = movedArray[i];
    for (let j = 0; j < row.length; j++) {
      let currentCell = movedArray[i][j];

      if (currentCell) {
        mergeCellsUp(movedArray, [i, j]);
      }
    }
  }
  // MOVE UP
  for (let i = 0; i < movedArray.length; i++) {
    const row = movedArray[i];
    for (let j = 0; j < row.length; j++) {
      if (!isCellFree(movedArray, [i, j]) && i !== 0) {
        movedArray = moveCellUp(movedArray, [i, j]);
      }
    }
  }

  return movedArray;
}

// moves a given cell to the corresponding cell in top row
export function moveCellUp(grid, [rowIndex, cellIndex]) {
  // copy the current grid
  let grid2 = copyMultidimensionalArray(grid);

  // get the Top Empty Cells Index
  const targetCellIndex = getHighestFreeCellInColumn(grid2, [
    rowIndex,
    cellIndex,
  ]);

  // set the target cells value
  const tileValue = grid[rowIndex][cellIndex];
  grid2[targetCellIndex[0]][cellIndex] = tileValue;

  // Remove tiles value
  if (rowIndex !== targetCellIndex[0]) {
    grid2[rowIndex][cellIndex] = null;
  }
  return grid2;
}

export function getHighestFreeCellInColumn(grid, [rowIndex, cellIndex]) {
  for (let i = rowIndex; i >= 0; i--) {
    if (rowIndex === 0 && isCellFree(grid, [i, cellIndex])) {
      return [i, cellIndex];
    }
    // next, if its any row, and any cell above is full, return index before full cell
    if (!isCellFree(grid, [i - 1, cellIndex])) {
      return [i, cellIndex];
    }
    // next, if its any row, and above cell is (empty && row 1) -> return row 1 index
    if (isCellFree(grid, [i - 1, cellIndex]) && i === 1) {
      return [i - 1, cellIndex];
    }
  }
}

// ******************
//     MOVE DOWN
// ******************

export function getLowestFreeCellInColumn(grid, [rowIndex, cellIndex]) {
  for (let i = rowIndex; i <= grid.length - 1; i++) {
    if (rowIndex === grid.length - 1) {
      return [i, cellIndex];
    }
    // next, if its any row, and any cell below is full, return index before full cell
    if (!isCellFree(grid, [i + 1, cellIndex])) {
      return [i, cellIndex];
    }
    // next, if its any row, and above cell is (empty && row 1) -> return row 1 index
    if (isCellFree(grid, [i + 1, cellIndex]) && i === 2) {
      return [i + 1, cellIndex];
    }
  }
}

export function moveCellDown(grid, [rowIndex, cellIndex]) {
  // copy the current grid
  let grid2 = copyMultidimensionalArray(grid);

  // get the Bottom Empty Cells Index
  const targetCellIndex = getLowestFreeCellInColumn(grid2, [
    rowIndex,
    cellIndex,
  ]);

  // set the target cells value
  const tileValue = grid[rowIndex][cellIndex];
  grid2[targetCellIndex[0]][cellIndex] = tileValue;

  // Remove tiles value
  if (rowIndex !== targetCellIndex[0]) {
    grid2[rowIndex][cellIndex] = null;
  }
  return grid2;
}

export function mergeCellsDown(grid, [rowIndex, cellIndex]) {
  const cellValue = grid[rowIndex][cellIndex];

  for (let n = rowIndex - 1; n >= 0; n--) {
    if (cellValue && cellValue !== grid[n][cellIndex] && grid[n][cellIndex]) {
      break;
    } else if (cellValue === grid[n][cellIndex]) {
      grid[rowIndex][cellIndex] = cellValue + grid[n][cellIndex];
      grid[n][cellIndex] = null;
    }
  }
  return grid;
}

export function moveDown(grid) {
  let grid2 = copyMultidimensionalArray(grid);

  const start = grid2.length - 1;
  const end = 0;

  for (let i = start; i >= end; i--) {
    const row = grid2[i];
    for (let j = 0; j < row.length; j++) {
      let currentCell = grid2[i][j];

      if (currentCell) {
        mergeCellsDown(grid2, [i, j]);
      }
    }
  }
  // MOVE UP
  for (let i = grid2.length - 1; i >= 0; i--) {
    const row = grid2[i];
    for (let j = 0; j < row.length; j++) {
      if (!isCellFree(grid2, [i, j])) {
        grid2 = moveCellDown(grid2, [i, j]);
      }
    }
  }

  return grid2;
}

export function checkGridsAreEqual(grid1, grid2) {
  return JSON.stringify(grid1) === JSON.stringify(grid2);
}

// ******************
//     MOVE LEFT
// ******************

export function getLeftmostFreeCellInRow(arr, [rowIndex, cellIndex]) {
  for (let i = cellIndex; i >= 0; i--) {
    // if its column 0, return current index
    if (cellIndex === 0) {
      return [i, cellIndex];
    }
    // next, if its any column, and any cell left is full, return index before full cell
    if (!isCellFree(arr, [rowIndex, i - 1])) {
      return [rowIndex, i];
    }
    // next, if its any row, and left cell is (empty && row 1) -> return row 1 index
    if (isCellFree(arr, [rowIndex, i - 1]) && i === 1) {
      return [rowIndex, i - 1];
    }
  }
}

export function moveCellLeft(grid, [rowIndex, cellIndex]) {
  // copy the current grid
  let grid2 = copyMultidimensionalArray(grid);

  // get the Leftmost Empty Cells Index
  const targetIndex = getLeftmostFreeCellInRow(grid2, [rowIndex, cellIndex]);

  // set the target cells value
  const tileValue = grid[rowIndex][cellIndex];
  grid2[rowIndex][targetIndex[1]] = tileValue;

  // Remove tiles value
  if (cellIndex !== targetIndex[1]) {
    grid2[rowIndex][cellIndex] = null;
  }
  return grid2;
}

export function mergeCellsLeft(grid, [rowIndex, cellIndex]) {
  let cellValue = grid[rowIndex][cellIndex];

  for (let n = cellIndex + 1; n < grid.length; n++) {
    if (cellValue && cellValue !== grid[rowIndex][n] && grid[rowIndex][n]) {
      break;
    } else if (cellValue === grid[rowIndex][n]) {
      grid[rowIndex][cellIndex] = cellValue + grid[rowIndex][n];
      grid[rowIndex][n] = null;
      break;
    }
  }
  return grid;
}

export function moveLeft(grid) {
  let movedArray = copyMultidimensionalArray(grid);

  const start = 0;
  const end = movedArray.length;

  for (let i = start; i < end; i++) {
    for (let j = 0; j < end; j++) {
      let currentCell = movedArray[j][i];

      if (currentCell) {
        mergeCellsLeft(movedArray, [j, i]);
      }
    }
  }
  // MOVE UP
  for (let i = 0; i < end; i++) {
    for (let j = 0; j < end; j++) {
      if (!isCellFree(movedArray, [j, i]) && i !== 0) {
        movedArray = moveCellLeft(movedArray, [j, i]);
      }
    }
  }

  return movedArray;
}

// ******************
//     MOVE Right
// ******************

export function getRightmostFreeCellInRow(grid, [rowIndex, cellIndex]) {
  for (let i = cellIndex; i <= grid.length - 1; i++) {
    // if its column 0, return current index
    if (cellIndex === grid.length - 1) {
      return [i, cellIndex];
    }
    // next, if its any column, and any cell right is full, return index before full cell
    if (!isCellFree(grid, [rowIndex, i + 1])) {
      return [rowIndex, i];
    }
    // next, if its any row, and right cell is (empty && row 1)
    if (isCellFree(grid, [rowIndex, i + 1]) && i === grid.length - 2) {
      return [rowIndex, i + 1];
    }
  }
}

export function moveCellRight(grid, [rowIndex, cellIndex]) {
  // copy the current grid
  let grid2 = copyMultidimensionalArray(grid);

  // get the Leftmost Empty Cells Index
  const targetIndex = getRightmostFreeCellInRow(grid2, [rowIndex, cellIndex]);

  // set the target cells value
  const tileValue = grid[rowIndex][cellIndex];
  grid2[rowIndex][targetIndex[1]] = tileValue;

  // Remove tiles value
  if (cellIndex !== targetIndex[1]) {
    grid2[rowIndex][cellIndex] = null;
  }
  return grid2;
}

export function mergeCellsRight(grid, [rowIndex, cellIndex]) {
  let cellValue = grid[rowIndex][cellIndex];

  for (let n = cellIndex - 1; n >= 0; n--) {
    if (cellValue && cellValue !== grid[rowIndex][n] && grid[rowIndex][n]) {
      break;
    } else if (cellValue === grid[rowIndex][n]) {
      grid[rowIndex][cellIndex] = cellValue + grid[rowIndex][n];
      grid[rowIndex][n] = null;
      break;
    }
  }
  return grid;
}

export function moveRight(grid) {
  let movedArray = copyMultidimensionalArray(grid);

  const start = movedArray.length - 1;
  const end = 0;

  for (let i = start; i >= end; i--) {
    for (let j = start; j >= end; j--) {
      let currentCell = movedArray[j][i];

      if (currentCell) {
        mergeCellsRight(movedArray, [j, i]);
      }
    }
  }
  // MOVE RIGHT
  for (let i = start; i >= end; i--) {
    for (let j = start; j >= end; j--) {
      if (!isCellFree(movedArray, [j, i]) && i !== start) {
        movedArray = moveCellRight(movedArray, [j, i]);
      }
    }
  }

  return movedArray;
}

export function createEmptyGrid() {
  return [
    [null, null, null, null],
    [null, null, null, null],
    [null, null, null, null],
    [null, null, null, null],
  ];
}
export function checkGameOver(grid) {
  const movedUp = moveUp(grid);
  const movedDown = moveDown(grid);
  const movedLeft = moveLeft(grid);
  const movedRight = moveRight(grid);

  return (
    checkGridsAreEqual(grid, movedUp) &&
    checkGridsAreEqual(grid, movedDown) &&
    checkGridsAreEqual(grid, movedLeft) &&
    checkGridsAreEqual(grid, movedRight)
  );
}
